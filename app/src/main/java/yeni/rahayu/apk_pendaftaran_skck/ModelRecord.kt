package yeni.rahayu.apk_pendaftaran_skck

class ModelRecord (
    var id:String,
    var name:String,
    var image: String,
    var desk : String,
    var phone:String,
    var alamat : String,
    var ttl : String,
    var addedTime : String,
    var updateTime : String

)