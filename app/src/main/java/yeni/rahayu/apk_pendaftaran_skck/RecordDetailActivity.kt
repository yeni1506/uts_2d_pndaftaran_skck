package yeni.rahayu.apk_pendaftaran_skck

import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_record_detail.*
import java.util.*

class RecordDetailActivity : AppCompatActivity() {

    private var actionBar: ActionBar? = null
    private var dbHelper: MyDbHelper? = null
    private var recordId: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record_detail)

        actionBar = supportActionBar
        actionBar!!.title = "Record Details"
        actionBar!!.setDisplayShowHomeEnabled(true)
        actionBar!!.setDisplayHomeAsUpEnabled(true)

        dbHelper = MyDbHelper(this)

        val intent = intent
        recordId = intent.getStringExtra("RECORD ID")

        showRecordDetails()

    }

    private fun showRecordDetails() {
        val selectQuery =
            "SELECT * FROM ${Constants.TABLE_NAME}WHERE${Constants.C_ID}=\"$recordId\""

        val db = dbHelper!!.writableDatabase
        val cursor = db.rawQuery(selectQuery, null)
        if (cursor.moveToFirst()) {
            do {

                val id = "" + cursor.getInt(cursor.getColumnIndex(Constants.C_ID))
                val name = "" + cursor.getString(cursor.getColumnIndex(Constants.C_NAME))
                val image = "" + cursor.getString(cursor.getColumnIndex(Constants.C_IMAGE))
                val desk = "" + cursor.getString(cursor.getColumnIndex(Constants.C_BIO))
                val phone = "" + cursor.getString(cursor.getColumnIndex(Constants.C_PHONE))
                val alamat = "" + cursor.getString(cursor.getColumnIndex(Constants.C_EMAIL))
                val ttl = "" + cursor.getString(cursor.getColumnIndex(Constants.C_DOB))
                val addedTimestamp =
                    "" + cursor.getString(cursor.getColumnIndex(Constants.C_ADDED_TIMESTAMP))
                val UpdateTimestamp =
                    "" + cursor.getString(cursor.getColumnIndex(Constants.C_UPDATED_TIMESTAMP))


                val calendar1 = Calendar.getInstance(Locale.getDefault())
                calendar1.timeInMillis = addedTimestamp.toLong()
                val timeAdded =
                    android.text.format.DateFormat.format("dd/MM/yyyy hh:mm:aa", calendar1)

                val calendar2 = Calendar.getInstance(Locale.getDefault())
                calendar2.timeInMillis = UpdateTimestamp.toLong()
                val timeUpdated =
                    android.text.format.DateFormat.format("dd/MM/yyyy hh:mm:aa", calendar2)

                nameTv.text = name
                bioTv.text = desk
                phoneTv.text = phone
                almTv.text = alamat
                dobTv.text = ttl
                addedDateTv.text = timeAdded
                updateDateTv.text = timeUpdated

                if (image == "null") {
                    profileIv.setImageResource(R.drawable.person)
                } else {
                    profileIv.setImageURI(Uri.parse(image))
                }


            } while (cursor.moveToNext())
        }
            db.close()

    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}