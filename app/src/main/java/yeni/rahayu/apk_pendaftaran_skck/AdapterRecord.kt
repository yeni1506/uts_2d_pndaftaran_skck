package yeni.rahayu.apk_pendaftaran_skck

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

//adapter class for recycle view
class AdapterRecord(): RecyclerView.Adapter<AdapterRecord.HolderRecord>(){

    private var context: Context?=null
    private var recordList:ArrayList<ModelRecord>?=null

    constructor(context: Context?, recordList: ArrayList<ModelRecord>?) : this(){
        this.context = context
        this.recordList = recordList

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderRecord {
        return HolderRecord(
            LayoutInflater.from(context).inflate(R.layout.row_record, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return recordList!!.size

    }

    override fun onBindViewHolder(holder: HolderRecord, position: Int) {
        val model = recordList!!.get(position)

        val id = model.id
        val name = model.name
        val image = model.image
        val desk = model.desk
        val phone = model.phone
        val alamat = model.alamat
        val ttl = model.ttl
        val addedTime = model.addedTime
        val updatedTime = model.updateTime

        //set data to views
        holder.nameTv.text = name
        holder.phoneTv.text = phone
        holder.almTv.text = alamat
        holder.dobTv.text = ttl

        if (image == "null"){
            holder.profileIv.setImageResource(R.drawable.person)
        }
        else{
            holder.profileIv.setImageURI(Uri.parse(image))
        }

        holder.itemView.setOnClickListener{
            val intent = Intent(context, RecordDetailActivity::class.java)
            intent.putExtra("RECORD_ID", id)
            context!!.startActivity(intent)
        }
        holder.moreBtn.setOnClickListener {
            showMoreOption(
                position,
                id,
                name,
                phone,
                alamat,
                ttl,
                desk,
                image,
                addedTime,
                updatedTime

                )

        }

    }

    private fun showMoreOption(
        position: Int,
        id: String,
        name: String,
        phone: String,
        alamat: String,
        dob: String,
        bio:String,
        image: String,
        addedTime: String,
        updatedTime: String
    ) {
        //option to display
        val option = arrayOf("EDIT, DELETE")
        val dialog:AlertDialog.Builder = AlertDialog.Builder(context)
        //dialog
        dialog.setItems(option){dialog, which ->
            //handle item cliked
            if(which==0){
                //edit clicked
                val intent = Intent(context, AddUpdateRecordActivity::class.java)
                intent.putExtra("ID",id)
                intent.putExtra("NAME",name)
                intent.putExtra("PHONE",phone)
                intent.putExtra("ALAMAT",alamat)
                intent.putExtra("TTL",dob)
                intent.putExtra("DESK",bio)
                intent.putExtra("ADDED_TIME",addedTime)
                intent.putExtra("UPDATED_TIME",updatedTime)
                intent.putExtra("IsEditMode",true)
                context!!.startActivity(intent)

            }
            else{

            }

        }
            dialog.show()

    }

    inner class  HolderRecord(itemView: View): RecyclerView.ViewHolder(itemView) {

        //views from row_record
        var profileIv: ImageView = itemView.findViewById(R.id.profileIv)
        var nameTv: TextView= itemView.findViewById(R.id.nameTv)
        var phoneTv: TextView= itemView.findViewById(R.id.phoneTv)
        var almTv: TextView= itemView.findViewById(R.id.almTv)
        var dobTv: TextView= itemView.findViewById(R.id.dobTv)
        var moreBtn: ImageButton= itemView.findViewById(R.id.moreBtn)

    }
}